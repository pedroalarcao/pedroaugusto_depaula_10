import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;

public class E22_7 extends Thread {

    private String filename;
    int countWord = 0;
    String line;

    public E22_7(String fname) {

        filename = fname;

    }

    @Override
    public void run() {
        try {

            File file = new File(filename);
            FileInputStream fileStream = new FileInputStream(file);
            InputStreamReader input = new InputStreamReader(fileStream);
            BufferedReader reader = new BufferedReader(input);

            while ((line = reader.readLine()) != null) {
                if (!(line.equals(""))) {

                    // \\s+ is the space delimiter in java
                    String[] wordList = line.split("\\s+");

                    countWord += wordList.length;

                }
            }

            System.out.println(filename + " : " + countWord);

            WordCountRun.totalCount += countWord;

        } catch (FileNotFoundException e) {

            e.printStackTrace();
            System.out.println("File " + filename + " not found.");

        } catch (Exception e) {

            e.printStackTrace();

        }
    }
}

class WordCountRun {


    static int totalCount = 0;

    public static void main(String[] args) {

        E22_7 words[] = new E22_7[args.length];

        try {
            if (args.length == 0) {

                throw new IllegalArgumentException();

            } else {
                for (int i = 0; i < args.length; i++) {

                    words[i] = new E22_7(args[i]);
                    words[i].run();

                }

                System.out.println("Combined count: " + totalCount);

            }
        } catch (IllegalArgumentException e) {

            e.printStackTrace();
            System.out.println("No arguments have been passed in main function");

        } catch (Exception e) {

            e.printStackTrace();
        }
    }
}