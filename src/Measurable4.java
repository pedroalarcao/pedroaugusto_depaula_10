public class Measurable4<T extends Number> {

    public final T obj;

    public Measurable4(T obj) {
        this.obj = obj;
    }

    public double doubleValue(){
        return obj.doubleValue();
    }
    public T getLargest() {

        return obj;

    }
}