import java.util.ArrayList;

class PairUtil2 {

    public static <T extends Measurable4> Pair <T, T>  largest(ArrayList<T> arr) {

            T max = arr.get(0);
            for (T t : arr) {
                if (t.doubleValue() > max.doubleValue()) {
                    max = t;
                }
            }
        return new Pair<>(max, max);
        }
    }
public class P18_4 {
    public static void main(String[] args) {
        ArrayList<Measurable4> arr = new ArrayList<>();

        arr.add(new Measurable4<>(3));
        arr.add(new Measurable4<>(55.5));
        arr.add(new Measurable4<>(200));
        arr.add(new Measurable4<>(99));
        Pair<Measurable4, Measurable4> pair = PairUtil2.largest(arr);
        System.out.println("Max: " + pair.getSecond().getLargest());
    }

    }
