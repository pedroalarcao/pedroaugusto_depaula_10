public class Measurable2<T extends Number> implements Measurable{

    public final T obj;

    public Measurable2(final T obj){

        this.obj = obj;

    }
    public double getMeasure() {

        return obj.doubleValue();
    }
}