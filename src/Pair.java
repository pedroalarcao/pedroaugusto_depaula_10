public class Pair<T,S>{

    private final T first;
    private final S second;

    public Pair(T firstElement, S secondElement){

        first = firstElement;
        second = secondElement;

    }

    public T getFirst() {return first;}

    public S getSecond() {return second;}
}
