

class PairUtil{
    public static <T extends Measurable> Pair <T, T> minmax(T[]arr){

        T min = arr[0], max = arr[0];

        for (T t : arr) {
            if (t.getMeasure() > max.getMeasure()) {
                max = t;
            }
            if (t.getMeasure() < min.getMeasure()) {
                min = t;
            }
        }
        return new Pair<>(min, max);
    }
}

public class P18_1 {
    public static void main(String[] args) {
        Measurable[] arr = new Measurable[4];

        arr[0] = new Measurable2<Integer>(3);
        arr[1] = new Measurable2<Double>(5.5);
        arr[2] = new Measurable2<Double>(99.9);
        arr[3] = new Measurable2<Integer>(1);
        Pair<Measurable, Measurable> pair = PairUtil.minmax(arr);
        System.out.println("Min: " + pair.getFirst().getMeasure());
        System.out.println("Max: " + pair.getSecond().getMeasure());
    }
}